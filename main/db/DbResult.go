package db

type Result struct {
	Qty int
	Rows []Row
}

type Row struct {
	ID int64
	User string
	Db string
	Host string
	Time string
	Query string
	Qty int
}

func NewRow(Id int64, User string, Db string, Host string) Row {
	return Row{ID:Id, User:User, Db:Db, Host:Host}
}

func (row *Row) setQty(qty int) {
	row.Qty = qty
}

func (row *Row) setQuery(query string) {
	row.Query = query
}

func (row *Row) setTime(time string) {
	row.Time = time
}

func (res *Result) AddNewRow(row Row)  {
	res.Rows = append(res.Rows, row)
}