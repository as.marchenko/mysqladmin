package proc

import (
	"fmt"
	"gitlab.com/as.marchenko/mysqladmin/main/db"
	"log"
	"time"
)

func AdminConnections() {

	for {
		result := db.GetConnectionsStat()
		log.Println("Connected: ", result.Qty)

		for _, row := range result.Rows {
			if row.Qty > 0 {
				fmt.Print("\t", row.Qty, ": User='", row.User, "', Db='", row.Db, "', From='", row.Host, "'\n")
			}
		}
		fmt.Println()
		time.Sleep(10 * time.Second)
	}
}