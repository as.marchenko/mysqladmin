module gitlab.com/as.marchenko/mysqladmin

go 1.16

require (
	gitlab.com/am.driver/gosql v1.0.4
	gitlab.com/am.driver/goutils v1.0.1
)
