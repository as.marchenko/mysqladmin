package proc

import (
	"fmt"
	"gitlab.com/am.driver/gosql"
	"gitlab.com/as.marchenko/mysqladmin/main/db"
	"log"
	"strconv"
	"strings"
	"time"
)

func RecreateTableByIDs(databaseName, tableName string, fields []string) {

	if len(tableName) == 0 || len(fields) == 0 { panic("Table or Fields not set..") }

	tableNameNew := "tmp_" + tableName + "_tmp"
	newTableExists, err0 := db.Connection().Admin().CheckTableExists(tableNameNew, databaseName)
	if err0 != nil {
		panic(err0)
	}

	if newTableExists {
		log.Println("Table already exists: " + tableNameNew)
	} else {
		createTable(databaseName, tableName, tableNameNew)
		log.Println("Table created: " + tableNameNew)
	}

	selectMD := "SELECT * FROM " + tableName + " LIMIT 1"
	rs, err := db.Connection().Select(selectMD)

	if err != nil { panic(err.String()) }

	insertSelect := "INSERT INTO `" + tableNameNew + "`("
	if rs.Next() {
		rsmd := rs.GetMetaData()
		for i := 1; i <= rsmd.GetColumnCount(); i++ {
			insertSelect = insertSelect + "`" + rsmd.GetColumnName(i) + "`"
			if i < rsmd.GetColumnCount() {
				insertSelect = insertSelect + ","
			}
		}
	}
	whereFields := make([]string, len(fields))
	for i := 0; i < len(fields); i++ {
		whereFields[i] = "`" + fields[i] + "`=?"
	}
	insertSelect = insertSelect + ") SELECT * FROM `" + tableName + "` WHERE " + strings.Join(whereFields, " AND ") + ";"

	log.Println("INSERT query prepared: ")
	fmt.Println(insertSelect)


	handler := db.Connection().CreateMySQLHandler(tableName)
	defer handler.Close()

	log.Println("Ready to start")
	time.Sleep(10 * time.Second)

	result := 0
	for handler.HasNext() {
		rsh := handler.Next("", fields[0], 1000)
		for rsh.Next() {
			insertQuery := strings.Replace(insertSelect, "?", gosql.PrepareStringValue(rsh.GetString(fields[0])), 1)
			if len(fields) > 1 {
				for i := 1; i < len(fields); i++ {
					insertQuery = strings.Replace(insertQuery, "?", gosql.PrepareStringValue(rsh.GetString(fields[i])), 1)
				}
			}
			_, err := db.Connection().ExecuteInsert(insertQuery)
			if err != nil {
				panic(err.String())
			}
			result++

			if result%1000 == 0 {
				log.Println("Processed: " + strconv.Itoa(result))
			}
		}
	}
	log.Println("Processed: " + strconv.Itoa(result))
}

func createTable(databaseName, tableName, tableNameNew string) {
	resTbl, err := db.Connection().Select(fmt.Sprintf("SHOW CREATE TABLE `%s`.`%s`;", databaseName, tableName))
	if err != nil {
		panic(err.String())
	}
	if resTbl.Next() {
		createTableSQL := strings.Replace(resTbl.GetString(2), tableName, tableNameNew, 1)
		if strings.Contains(strings.ToUpper(createTableSQL), "CONSTRAINT") {
			i := 1
			for _, line := range strings.Split(createTableSQL, ",") {
				if strings.Contains(strings.ToUpper(line), "CONSTRAINT") {
					constraintName := tableName + "_" + strconv.FormatInt(time.Now().Unix(), 10) + "_" + strconv.Itoa(i)
					strReplace := line[strings.Index(strings.ToUpper(line), "CONSTRAINT") : strings.Index(strings.ToUpper(line), "FOREIGN KEY")]
					createTableSQL = strings.Replace(createTableSQL, strReplace, "CONSTRAINT `" + constraintName + "` ", 1)
					i++
				}
			}
		}
		_, err := db.Connection().ExecuteUpdate(createTableSQL)
		if err != nil {
			panic(err.String())
		}
	} else {
		panic("Table not found: " + tableName)
	}
}