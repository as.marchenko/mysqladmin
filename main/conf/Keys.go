package conf

import (
	"strconv"
	"strings"
)

type Keys struct {
	Trx bool
	TrxLong int
	FullQueries bool
	RecreateTable string
	RecreateTableFields []string
	DmpUsers bool
}

func (keys *Keys) SetKey(key string) {
	key = strings.Replace(key, "--", "", 1)
	parts := strings.Split(key, "=")

	switch parts[0] {
		case "trx":
			keys.Trx = true
		case "trx-long":
			trxLong, _ := strconv.Atoi(parts[1])
			keys.TrxLong = trxLong
		case "full-queries":
			keys.FullQueries = true
		case "dmp-users":
			keys.DmpUsers = true
		case "table-recreate":
			keys.RecreateTable = parts[1]
		case "table-recreate-fields":
			keys.RecreateTableFields = strings.Split(parts[1], ",")
		default:
			panic("Unknown key:" + parts[0])
	}
}