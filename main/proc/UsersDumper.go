package proc

import (
	"fmt"
	"gitlab.com/as.marchenko/mysqladmin/main/db"
	"strings"
)

func DumpUsers()  {
	for _, row := range db.DumpUsers() {
		if !strings.HasSuffix(row, ";") {
			row = row + ";"
		}
		fmt.Println(row)
	}
}