package db

import (
	"fmt"
	"gitlab.com/am.driver/gosql"
	"strings"
)

var con gosql.MySQLConnection

func InitConnection(host, user, pass, database string)  {

	var err error
	con, err = gosql.NewMySQLConnection(host, 3306, user, pass, database)

	if err != nil {
		fmt.Println(err)
		panic("Cant connect to database")
	}
}

func Connection() *gosql.MySQLConnection {
	return &con
}

func GetConnectionsStat() (ret Result) {
	rs, err := con.Select("SELECT COUNT(*) FROM `PROCESSLIST`")

	if err == nil && rs.Next() {
		ret.Qty = rs.GetInt(1)
	} else {
		fmt.Println(err)
	}

	sel := "SELECT `USER` AS c_user, `db` AS c_db, SUBSTRING_INDEX(`HOST`,':',1) AS c_host, COUNT(*) AS cnt " +
			"FROM `PROCESSLIST` WHERE `USER` NOT IN ('system user', 'event_scheduler') " +
			"GROUP BY 1 , 2, 3 ORDER BY 4 DESC;"

	rs, err = con.Select(sel)

	if err == nil {
		for rs.Next() {
			row := NewRow(0, rs.GetString("c_user"), rs.GetString("c_db"), rs.GetString("c_host"))
			row.setQty(rs.GetInt("cnt"))

			ret.AddNewRow(row)
		}
	} else {
		fmt.Println(err)
	}

	return ret
}


func GetTransactions(length int) (ret Result) {

	sel := "SELECT `ID`, `USER`, `DB`, SUBSTRING_INDEX(`HOST`,':',1) AS t_host, TIMEDIFF(NOW(),trx_started) AS 't_time', INFO " +
			"FROM information_schema.`PROCESSLIST` p " +
			"JOIN information_schema.`INNODB_TRX` t ON p.ID=t.trx_mysql_thread_id " +
			"HAVING t_time > SEC_TO_TIME(?) ORDER BY t.trx_started;"

	rs, err := con.Select(sel, length)

	if err == nil {
		for rs.Next() {
			row := NewRow(rs.GetLong("ID"), rs.GetString("USER"), rs.GetString("DB"), rs.GetString("t_host"))
			row.setTime(rs.GetString("t_time"))
			row.setQuery(string(rs.GetBytes("INFO")))

			ret.AddNewRow(row)
		}
	} else {
		fmt.Println(err)
	}

	return ret
}

func DumpUsers() (ret []string) {

	sel := "SELECT `Host` AS vHost, `User` AS vUser, `Password` AS vPass FROM mysql.user WHERE `User` NOT IN ('root', '');"
	rs, err := con.Select(sel)

	if err == nil {
		for rs.Next() {
			show := "SHOW GRANTS FOR `" + rs.GetString("vUser") + "`@`" + rs.GetString("vHost") + "`;"
			rsg, err := con.Select(show)
			if err == nil {
				if rsg.Next() {
					grantRes := rsg.GetString(1)
					if strings.Contains(grantRes, "IDENTIFIED BY") {
						grantRes = strings.Split(grantRes, "IDENTIFIED BY")[0]
					}

					createSQL := "CREATE USER `" + rs.GetString("vUser") + "`@`" + rs.GetString("vHost") + "` IDENTIFIED WITH mysql_native_password BY 'somepassword';"
					updatePassSQL := "UPDATE `mysql`.`user` SET authentication_string='" + rs.GetString("vPass") + "' WHERE `User`='" + rs.GetString("vUser") + "' AND `Host`='" + rs.GetString("vHost") + "';"

					ret = append(ret, createSQL)
					ret = append(ret, updatePassSQL)
					ret = append(ret, grantRes)
				} else {
					fmt.Println("Grants not found for: ", rs.GetString("vUser"))
				}
			} else {
				fmt.Println("Show grants: ", err)
			}
		}
	} else {
		fmt.Println(err)
	}

	return ret
}

