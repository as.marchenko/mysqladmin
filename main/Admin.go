package main

import (
	"fmt"
	"gitlab.com/as.marchenko/mysqladmin/main/conf"
	"gitlab.com/as.marchenko/mysqladmin/main/db"
	"gitlab.com/as.marchenko/mysqladmin/main/proc"
	"os"
	"strings"
)

var host, user, pass, database string
var keys conf.Keys

func main() {

	loadParams()

	db.InitConnection(host, user, pass, database)
	if keys.Trx {
		proc.AdminTransactions(keys.TrxLong, keys.FullQueries)
	} else if keys.DmpUsers {
		proc.DumpUsers()
	} else if len(keys.RecreateTable) > 0 {
		proc.RecreateTableByIDs(database, keys.RecreateTable, keys.RecreateTableFields)
	} else {
		proc.AdminConnections()
	}
}

func loadParams() {

	user = "root"
	host = "localhost"
	database = "information_schema"

	if len(os.Args) > 0 {
		for i, arg := range os.Args {
			if strings.HasPrefix(arg, "--") {
				keys.SetKey(arg)

			} else if strings.HasPrefix(arg, "-") {
				arg = strings.Replace(arg, "-", "", 1)

				bArg := []rune(arg)
				switch string(bArg[0:1]) {
					case "p":
						if len(arg) > 1 {
							pass = string(bArg[1:])
						} else {
							pass = os.Args[i+1]
						}
					case "u", "U":
						if len(arg) > 1 {
							user = string(bArg[1:])
						} else {
							user = os.Args[i+1]
						}
					case "d", "D":
						database = os.Args[i+1]
					case "h", "H":
						host = os.Args[i+1]
					default:
						panic("Unknown argument: " + arg)
				}
			}
		}
	}

	if len(pass) == 0 {
		printHelpAndExit()
	}
}

func printHelpAndExit() {
	fmt.Println("[ERROR] Try to setup connection properties:")
	fmt.Println("  -h - database host (localhost by default)")
	fmt.Println("  -u - user login (root by default)")
	fmt.Println("  -p - user password")
	fmt.Println("  -db - database")
	fmt.Println("  --trx - show long transactions")
	fmt.Println("  --trx-long - transactions long (seconds)")
	fmt.Println("  --full-queries - default will be logged  only first 150 symbols")
	fmt.Println("  --dmp-users - migrate users from mysql 5.0 to 8.0 version")
	fmt.Println("  --table-recreate - recreate table") // free disk space
	fmt.Println("  --table-recreate-fields - recreate table fields")
	os.Exit(1)
}