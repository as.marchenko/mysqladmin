package proc

import (
	"fmt"
	"gitlab.com/as.marchenko/mysqladmin/main/db"
	"log"
	"strings"
	"time"
)

func AdminTransactions(length int, fullQueries bool) {

	for {
		result := db.GetTransactions(length)

		if len(result.Rows) == 0 {
			log.Println("No transactions")
		} else {
			log.Println("Transactions: ", len(result.Rows))
			for _, row := range result.Rows {
				fmt.Print("\tID=", row.ID, ", User='", row.User, "', Db='", row.Db, "', From='", row.Host,
					"', Time=", row.Time, "\n")

				fmt.Print("\tID=", row.ID, ", Query: ", prepareQuery(row.Query, fullQueries), "\n")
			}
			fmt.Println()
		}

		time.Sleep(10 * time.Second)
	}
}

func prepareQuery(query string, isFull bool) string {

	if !isFull && len(query) > 150 {
		queryRunes := []rune(query)
		query = string(queryRunes[0:150]) + "..."
	}

	query = strings.Replace(query, "\t", " ", -1)
	query = strings.Replace(query, "\n", " ", -1)
	query = strings.Replace(query, "\r", " ", -1)
	query = strings.Replace(query, "\r\n", " ", -1)
	query = strings.Replace(query, "\n\r", " ", -1)

	for strings.Contains(query, "  ") {
		query = strings.Replace(query, "  ", " ", -1)
	}

	return query
}